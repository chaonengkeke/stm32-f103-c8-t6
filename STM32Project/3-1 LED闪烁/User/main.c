// #include "stm32f10x.h"                  // Device header
// #include "Delay.h"

// int main(void)
// {
// 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

// 	GPIO_InitTypeDef GPIO_InitStructure;
// 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
// 	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
// 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
// 	GPIO_Init(GPIOA, &GPIO_InitStructure);

// 	while (1)
// 	{
// 		GPIO_ResetBits(GPIOA, GPIO_Pin_0);
// 		Delay_ms(500);
// 		GPIO_SetBits(GPIOA, GPIO_Pin_0);
// 		Delay_ms(500);

// 		GPIO_WriteBit(GPIOA, GPIO_Pin_0, Bit_RESET);
// 		Delay_ms(500);
// 		GPIO_WriteBit(GPIOA, GPIO_Pin_0, Bit_SET);
// 		Delay_ms(500);

// 		GPIO_WriteBit(GPIOA, GPIO_Pin_0, (BitAction)0);
// 		Delay_ms(500);
// 		GPIO_WriteBit(GPIOA, GPIO_Pin_0, (BitAction)1);
// 		Delay_ms(500);
// 	}
// }

#include "stm32f10x.h"

void TIM3_PWM_Init(void)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructure.TIM_Period = 1000 - 1; // 自动重装载值
    TIM_TimeBaseStructure.TIM_Prescaler = 71;    // 预分频器
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0; // 占空比
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC3Init(TIM3, &TIM_OCInitStructure);

    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE); // 使能定时器中断
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;              // 定时器3中断
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01; // 抢占优先级1
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;        // 响应优先级1
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_Cmd(TIM3, ENABLE);
}

void delay_ms(uint16_t ms)
{
    uint16_t i, j;
    for (i = 0; i < ms; i++)
        for (j = 0; j < 1141; j++)
            ;
}

int main(void)
{
    uint16_t i;

    TIM3_PWM_Init();

    while (1)
        ;
}

void TIM3_IRQHandler(void)
{
    static uint16_t i = 0;
    static uint8_t flag = 0;

    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) // 检查TIM3更新中断发生与否
    {
        if (flag == 0) {
            i += 10; // 方波占空比增大，表现为灯变暗
            if (i >= 1000)
                flag = 1; // 转换状态
        } else if (flag == 1) {
            i -= 10; // 方波占空比减小，表现为灯变亮
            if (i <= 0)
                flag = 0; // 转换状态
        }
        TIM_SetCompare3(TIM3, i); // 设置PWM占空比
        delay_ms(5);
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update); // 清除TIMx更新中断标志
    }
}
