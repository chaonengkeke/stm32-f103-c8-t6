#include "stm32f10x.h"

/**
  * @brief  �?秒级延时
  * @param  xus 延时时长，范围：0~233015
  * @retval �?
  */
void Delay_us(uint32_t xus)
{
	SysTick->LOAD = 72 * xus;				//设置定时器重装�?
	SysTick->VAL = 0x00;					//清空当前计数�?
	SysTick->CTRL = 0x00000005;				//设置时钟源为HCLK，启动定时器
	while(!(SysTick->CTRL & 0x00010000));	//等待计数�?0
	SysTick->CTRL = 0x00000004;				//关闭定时�?
}

/**
  * @brief  �?秒级延时
  * @param  xms 延时时长，范围：0~4294967295
  * @retval �?
  */
void Delay_ms(uint32_t xms)
{
	while(xms--)
	{
		Delay_us(1000);
	}
}
 
/**
  * @brief  秒级延时
  * @param  xs 延时时长，范围：0~4294967295
  * @retval �?
  */
void Delay_s(uint32_t xs)
{
	while(xs--)
	{
		Delay_ms(1000);
	}
} 
