#include "Delay.h"
#include "stm32f10x.h" // Device header

int main(void)
{
    uint8_t flag = 1;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	// 引脚组,使能
    GPIO_InitTypeDef led_blink;						// 定义一个引脚对象
    led_blink.GPIO_Mode = GPIO_Mode_Out_PP;			// 引脚模式设置
    led_blink.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;	// 指定引脚
    led_blink.GPIO_Speed = GPIO_Speed_2MHz;			// 引脚最大频率
    GPIO_SetBits(GPIOB, GPIO_Pin_5);
    GPIO_Init(GPIOB, &led_blink);

    while (1) {
        if (flag == 1) {
            GPIO_ResetBits(GPIOB, GPIO_Pin_5);
            flag = 0;
        } else {
            GPIO_SetBits(GPIOB, GPIO_Pin_5);
            flag = 1;
        }

        GPIO_Write(GPIOA, ~0x0001); // 0000 0000 0000 0001 低电平有效
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0002); // 0000 0000 0000 0010
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0004); // 0000 0000 0000 0100
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0008); // 0000 0000 0000 1000
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0010); // 0000 0000 0001 0000
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0020); // 0000 0000 0010 0000
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0040); // 0000 0000 0100 0000
        Delay_ms(100);
        GPIO_Write(GPIOA, ~0x0080); // 0000 0000 1000 0000
        Delay_ms(100);
    }
}
